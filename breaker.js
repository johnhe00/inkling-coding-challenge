// (c) 2013 Inkling Systems, Inc.

/**
 * A breaker view handles rendering the chrome in the block-breaker challenge, as well as managing
 * overall game state.
 *
 * @param {Element} arenaEl The arena (rendering area).
 * @param {Element} levels The element containing level data.
 *
 * @constructor
 */
function BreakerView(arenaEl, levelsEl){
    this.el = arenaEl;

    // Extract container metrics. We ignore resizing, so don't do it!
    this.width = arenaEl.clientWidth;
    this.height = arenaEl.clientHeight;

    // Create the levels.
    var levels = levelsEl.querySelectorAll('level');
    this.levels = Array.prototype.slice.call(levels).map(function(el){
        return new Level(el);
    });

    this.el.addEventListener("mousemove", function(mouse) { view.updatePaddle(mouse) }, false);
    this.el.addEventListener("click", function(e) { view.play() }, false);

    // Set up initial game state.
    this.state = BreakerView.STATE.PAUSED;
    this.level = 0;
}

BreakerView.prototype.render = function(){
    this.el.innerHTML = '<div class="breaker-ui overlay">' +
        '    <span class="breaker-ui status">Click anywhere to begin.</span>' +
        '</div>';

    // Show the current level.
    this.renderLevel();

    // Begin our main loop.
    var anim = window.requestAnimationFrame || window.webkitRequestAnimationFrame ||
        window.mozRequestAnimationFrame;
    var frameWrapper = function(){
        this.frame();
        anim(frameWrapper);
    }.bind(this);
    frameWrapper();
};

/**
 * Renders a single frame of animation.
 */
BreakerView.prototype.frame = function(){
    if (this.state !== BreakerView.STATE.PLAYING) return;

    // Reposition the bouncer. This will return any collisions to us.
    var result = this.gamePieces.bouncer.reposition();

    if (result.type == Bouncer.COLLISION_TYPE.BRICK) {
        for (var i = 0; i < result.targets.length; i++) {
            this.damageBrick(result.targets[i]);
        }
        if (this.brickCount === 0) this.advanceLevel();
    }
    else if (result.type == Bouncer.COLLISION_TYPE.FLOOR) this.restartLevel();
};

/**
 * Pauses the game after the bouncer hits the floor. Sets the state to DEAD and shows the overlay.
 */
BreakerView.prototype.restartLevel = function(){
    this.state = BreakerView.STATE.DEAD;

    // Show the overlay.
    this.el.querySelector('.overlay').classList.remove('hidden');
};

/**
 * Pauses the game after the last brick in a level has been destroyed. Sets the state to PAUSED,
 * renders the next level, and shows the overlay.
 */
BreakerView.prototype.advanceLevel = function(){
    this.state = BreakerView.STATE.PAUSED;
    this.level++;
    if (this.level === this.levels.length) this.level = 0;
    this.clearArena();
    this.renderLevel();

    // Show the overlay.
    this.el.querySelector('.overlay').classList.remove('hidden');
};

/**
 * Damages the given brick. If the brick reaches 0 health, removes it from the arena.
 *
 * @param {Element} brick The brick to be damaged.
 */
BreakerView.prototype.damageBrick = function(brick){
    var health = parseInt(brick.dataset.health, 10);
    health--;
    brick.dataset.health = health;
    if (health === 0){
        this.brickCount--;
        this.el.removeChild(brick);
    }
};

/**
 * Renders the current level in the arena.
 */
BreakerView.prototype.renderLevel = function(){
    var level = this.levels[this.level];

    this.gamePieces = level.render(this.el);

    // Grab a count of how many bricks are in the arena. We will use this to track how many bricks
    // remain in the arena so we can avoid performing a querySelectorAll in the main animation
    // loop.
    this.brickCount = this.gamePieces.bricks.length;
};

/**
 * Updates the paddle by centering it on the given x value.
 *
 * @param {number} x The desired x coordinate of the center of the paddle relative to the screen.
 */
BreakerView.prototype.updatePaddle = function(mouse){
    var x = mouse.x || mouse.clientX;

    // Grab the offsetLeft of the containing element.
    var el = this.el;
    var offsetLeft = 0;
    while (el){
        offsetLeft += el.offsetLeft;
        el = el.offsetParent;
    }

    // Convert to coordinates within our arena.
    x -= this.el.offsetLeft;

    // Offset x so that it's aligned with the center of our paddle.
    var paddleWidth = this.gamePieces.paddle.clientWidth;
    x -= paddleWidth / 2;

    // Clamp x so that we won't push the paddle off the arena.
    var arenaRight = this.el.clientWidth - paddleWidth;
    if (x < 0) x = 0;
    if (x > arenaRight) x = arenaRight;

    // Finally, reposition the paddle!
    this.gamePieces.paddle.style.left = x + 'px';
};

/**
 * Removes all non-chrome items from the arena. Used to clean the arena of level data.
 */
BreakerView.prototype.clearArena = function(){
    var pieces = Array.prototype.slice.call(this.el.querySelectorAll(':not(.breaker-ui)'));
    pieces.forEach(function(piece){
        this.el.removeChild(piece);
    }, this);
};

/**
 * Starts the interactive part of a level. Updates the state to PLAYING and if the previous state
 * was DEAD, renders the previous arena again.
 */
BreakerView.prototype.play = function(){
    // Make sure we render this first arena if this is resuming from a lost game.
    if (this.state === BreakerView.STATE.DEAD){
        this.clearArena();
        this.renderLevel();
    }

    // Hide the overlay.
    this.el.querySelector('.overlay').classList.add('hidden');

    // Make sure our main loop knows we should be animating.
    this.state = BreakerView.STATE.PLAYING;
};

/**
 * An enum of possible game states.
 * @enum
 */
BreakerView.STATE = {
    // The game is paused before the next level.
    PAUSED: 0,
    // The game is active.
    PLAYING: 1,
    // The user let the ball hit the floor, so we'll repeat the same level.
    DEAD: 2
};

/**
 * Helper for processing block data/metadata and rendering the level.
 *
 * @param {Element} el The el containing the arena data.
 *
 * @constructor
 */
function Level(el){
    this.rows = Array.prototype.slice.call(el.querySelectorAll('row'));
    this.paddle = el.querySelector('paddle');
    this.bouncer = el.querySelector('bouncer');
}

/**
 * Renders this arena into the given element.
 *
 * @param {Element} parentEl The element to be rendered in.
 *
 * @return {Object} The rendered items:
 *       - {Element} paddle The paddle element.
 *       - {Bouncer} bouncer The bouncer made for this level.
 */
Level.prototype.render = function(parentEl){
    // Render the paddle.
    var paddle = document.createElement('div');
    paddle.classList.add('paddle');
    paddle.style.width = this.paddle.dataset.width + 'px';

    parentEl.appendChild(paddle);

    var y = Level.BRICK_SPACING;
    var x = Level.BRICK_SPACING;

    // Render each row.
    this.rows.forEach(function(row){
        var bricks = [];

        // Render the bricks in the row.
        Array.prototype.slice.call(row.querySelectorAll('brick')).forEach(function(brick){
            var brickEl = document.createElement('div');
            brickEl.classList.add('brick');
            brickEl.dataset.health = brick.dataset.health || 1;
            bricks.push(brickEl);
            if (brick.dataset.count){
                var count = parseInt(brick.dataset.count, 10) - 1;
                while (count--){
                    bricks.push(brickEl.cloneNode());
                }
            }
        });

        // Each brick should be the same size, leaving BRICK_SPACING worth of space between ach
        // brick and between the edge bricks and the sides of the arena.
        var brickWidth = parentEl.clientWidth - (Level.BRICK_SPACING * (1 + bricks.length));
        brickWidth /= bricks.length;
        bricks.forEach(function(brick){
            brick.style.top = y + 'px';
            brick.style.left = x + 'px';
            brick.style.height = Level.BRICK_HEIGHT + 'px';
            brick.style.width = brickWidth + 'px';
            parentEl.appendChild(brick);
            x += brickWidth + Level.BRICK_SPACING;
        });

        // Reset to the next row.
        y += Level.BRICK_HEIGHT + Level.BRICK_SPACING;
        x = Level.BRICK_SPACING;
    });

    // Remove dead bricks.
    var deadBricks = parentEl.querySelectorAll('.brick[data-health="0"]');
    Array.prototype.slice.call(deadBricks).forEach(function(brick){
        parentEl.removeChild(brick);
    });

    // Render the bouncer.
    var bouncer = new Bouncer(this.bouncer.dataset.width, this.bouncer.dataset.height,
        this.bouncer.dataset.speed);
    parentEl.appendChild(bouncer.el);
    bouncer.move(0, paddle.offsetTop - this.bouncer.dataset.height);

    return {
        paddle: paddle,
        bouncer: bouncer,
        bricks: parentEl.querySelectorAll('.brick')
    };
};

/**
 * The number of pixels between each brick and between a brick and the wall.
 * @type {number}
 */
Level.BRICK_SPACING = 5;

/**
 * The height of each brick in pixels.
 * @type {number}
 */
Level.BRICK_HEIGHT = 40;

/**
 * Handles bouncer-related logic. Performs ball movement and collision detection.
 *
 * @param {number} width The desired width of the bouncer.
 * @param {number} height The desired height of the bouncer.
 * @param {number} speed The desired magnitude of the velocity vector.
 *
 * @constructor
 */
function Bouncer(width, height, speed){
    this.el = document.createElement('div');
    this.el.classList.add('bouncer');
    this.el.style.width = width + 'px';
    this.el.style.height = height + 'px';

    // Compute a direction vector with the desired magnitude. Angle is 67.5 degrees.
    var angle = -67.5;
    angle = (angle / 180) * Math.PI;
    this.dx = speed * Math.cos(angle);
    this.dy = speed * Math.sin(angle);
    this.speed = speed;

    var colorIndex = Math.floor(Math.random() * Bouncer.COLORS.length);
    this.el.style.backgroundColor = Bouncer.COLORS[colorIndex];

    this.paddleHit = false;
}

/**
 * Update the bouncer's style to reposition it within the parent.
 */
Bouncer.prototype.move = function(x, y){
    this.x = x;
    this.y = y;
    this.el.style.left = x + 'px';
    this.el.style.top = y + 'px';
};

/**
 * Repositions the bouncer. Updates the bouncer based on its current velocity. If a collision
 * is detected with any sibling elements or with a wall, it will be returned.
 *
 * @return {Object} The results of the reposition with the following values:
 *       - {number} type All types of collisions from this reposition. This is a bitfield of values
 *           from Bouncer.COLLISION_TYPE.
 *       - {Array.<Element>} targets All elements involved in the collision.
 */
Bouncer.prototype.reposition = function(){
    var result = {
        type: Bouncer.COLLISION_TYPE.NONE,
        targets: []
    };

    var x = this.x + this.dx;
    var y = this.y + this.dy;

    // Points of collision
    var north = y;
    var west = x;
    var south = north + this.el.offsetHeight;
    var east = west + this.el.offsetWidth;

    // Boundaries
    if (south >= view.height) {
        result.type = Bouncer.COLLISION_TYPE.FLOOR;
    }
    else if (north <= 0) {
        this.dy = -this.dy;
        result.type = Bouncer.COLLISION_TYPE.WALL;
    }
    if (west <= 0 || east >= view.width) {
        this.dx = -this.dx;
        result.type = Bouncer.COLLISION_TYPE.WALL;
    }

    // Paddle
    if (!result.type) {
        var paddle = view.gamePieces.paddle;
        if (!this.paddleHit && south >= view.height - paddle.offsetHeight &&
            east >= paddle.offsetLeft &&
            west <= paddle.offsetLeft + paddle.offsetWidth) {
            this.dy = -this.dy;
            result.type = Bouncer.COLLISION_TYPE.PADDLE;
            this.paddleHit = true;
        }
    }

    // Bricks
    if (!result.type) {
        var collision = { x: 0, y: 0 };
        var bricks = view.gamePieces.bricks;
        for (var i = 0; i < bricks.length; i++) {
            var brick = bricks[i];
            var overlapY = Math.max(0, Math.min(brick.offsetTop + brick.offsetHeight, south) - Math.max(brick.offsetTop, north));
            var overlapX = Math.max(0, Math.min(brick.offsetLeft + brick.offsetWidth, east) - Math.max(brick.offsetLeft, west));
            if (overlapX > 0 && overlapY > 0) {
                collision.x += overlapX;
                collision.y += overlapY;
                result.targets.push(brick);
                result.type = Bouncer.COLLISION_TYPE.BRICK;
            }
        }

        if (collision.x > collision.y) {
            this.dy = -this.dy;
        }
        else if (collision.x < collision.y) {
            this.dx = -this.dx;
        }
        else if (collision.x > 0 && collision.y > 0) {
            this.dy = -this.dy;
        }
    }

    if (result.type != Bouncer.COLLISION_TYPE.NONE && result.type != Bouncer.COLLISION_TYPE.PADDLE) this.paddleHit = false;

    // Move to our final position.
    this.move(x, y);
    return result;
};

/**
 * Enum of possible collision types. Each represents a bit for the bitfield returned by reposition.
 * @enum
 */
Bouncer.COLLISION_TYPE = {
    NONE: 0,
    FLOOR: 1,
    WALL: 2,
    BRICK: 4,
    PADDLE: 8
};

/**
 * A set of colors bouncers may make themselves.
 * @type {Array.<string>}
 * @constant
 */
Bouncer.COLORS = [
    '#007db6', '#008fb2', '#009b9e', '#00a77d', '#00b247', '#5ab027', '#a0b61e'
];
