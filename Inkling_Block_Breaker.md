# JavaScript Engineer Challenge (block-breaker)

The included application contains a nearly-functional game. The goal of the game is to clear each brick from the screen. You are given a paddle (along the bottom of the arena) which will be centered on your mouse. The bouncing block will damage a brick if it collides with it, and will simply bounce off of walls and the paddle. If the bouncer hits the floor below your paddle, you will have to restart the level.

The included code is a nearly complete version of this game. You found this code and love the idea of the game, so you decided you'd finish it. In the JS code, there are several TODO comments that indicate what functionality is missing:

+ BreakerView's constructor looks like event handlers weren't added to remove the overlay, and that there's nothing listening for mouse events to update the paddle.
+ BreakerView.prototype.frame looks like it should be damaging bricks when a collision is seen, resetting the current level if the bouncer hits the floor, and advancing the level if all of the bricks have been destroyed.
+ Bouncer.prototype.reposition looks like it's just moving the bouncer but not actually checking if it runs into anything. Maybe some collision detection is in order?

This implementation only needs to function in the latest versions of Chrome, Firefox, or Safari.
